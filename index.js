// 2.
console.log('Hello World');

// 3-4.
let num1 = parseInt(prompt('Provide a number'))
let num2 = parseInt(prompt('Provide another number '))
let result = num1 + num2;
if(result < 10){
	console.warn('The total of two numbers is '+result);
}
else if(result >= 10 && result <=20){
	let difference = num1 - num2;
	alert('The difference of the two numbers are: ' +difference);
}
else if(result >= 21 && result <=29){
	let product = num1 * num2;
	alert('The product of the two numbers are: ' +product);
}
else {
	let quotient = num1 / num2;
	alert('The quotient of the two numbers are: ' +quotient);
}


// 5.
let name = prompt('Enter your name');
let age = prompt('Enter your age');

if(name === '' || age === ''){
	console.log('Are you a time traveler?');
}
else {
	
	console.log('Name: '+ name +'\nAge: ' + age);
}


// 6.
function isLegalAge(age){
	if(age >= 18){
		alert('You are of legal age.');
	}
	else{
		alert('You are not allowed here.');
	}
}
isLegalAge(age);

// 7.
switch(age){
	case 18:
	console.log('You are allowed to party.');
	break;

	case 21:
	console.log('You are now part of the adult society.');
	break;

	case 65:
	console.log('We thank you for your contribution to society');
	break;

	default :
	console.log('Are you sure you\'re not an alien?');
	break;
}

// 8.
try {
	alerat(isLegalAge(age));
} catch (error){
	console.log(typeof error);
	console.warn(error.message);
} finally {
	alert('Unexpected age input.');
}